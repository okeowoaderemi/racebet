# Instructions

### First run the following to install all the packages needed to run

`npm install`

### To run the project with a local server default port is 4200
`ng serve`

### How to Build a Production Build
`ng build --prod --base-href ''`
* It is quite important to set the Base Href as ''

### Run Tests
`npm test`
