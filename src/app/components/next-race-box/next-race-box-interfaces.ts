export interface IPurse {
  amount: number;
  currency: string;
}
export interface IRaceEvent {
  title: string;
  country: string;
}
export interface IRunners {
  id_runner: number;
  name: string;
  odds: number;
  silk: string;
}

export interface IRaceData {
  id_race: number;
  event: IRaceEvent;
  race_type: string;
  post_time: number;
  num_runners: number;
  distance: number;
  purse: IPurse;
  runners: Array<IRunners>;
}

export interface IRaceObject {
  races: Array<IRaceData>;
}
