import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { IRaceFilterStateTypes } from '../race-type-filter/race-type-filter.component';
import { environment } from '../../../environments/environment';
import { IRaceObject, IRaceData } from './next-race-box-interfaces';



@Injectable()
export class NextRaceBoxServices {
  private filterData: IRaceFilterStateTypes;
  private data: any;
  constructor(private http: HttpClient) {}
  load(): Observable<any> {
    return this.http.get('./next_races.json');
  }

  setFilterData(filterData: IRaceFilterStateTypes) {
    this.filterData = filterData;
  }

  getFiltetData() {
    return this.filterData;
  }
  // Get the Data and filter and return what we need
  transform(rawData: IRaceObject) {
    // Set the rules for transform
    const filterData = this.getFiltetData();
    const TransformRules = {
      T: filterData.isTrotFilterEnabled,
      G: filterData.isGallopFilterEnabled,
      J: filterData.isJumpFilterEnabled,
      D: filterData.isDogFilterEnabled
    };
    const newData = rawData.races.filter(
      (item, index, array) => TransformRules[item.race_type]
    );
    return newData.sort(this.sortAmount.bind(this))[0]; // Get the First item which should be the highest purse
  }

  setData(data: IRaceObject) {
    this.data = data;
  }

  getData(): IRaceObject {
    return this.data;
  }
  convertToEuro(amount: number) {
    return amount * environment.GBP_TO_EURO_RATE;
  }

  sortAmount(prev: IRaceData, next: IRaceData) {
    let prevAmount, nextAmount;
    if (prev.purse.currency === 'GBP') {
      prevAmount = this.convertToEuro(prev.purse.amount);
    } else {
      prevAmount = prev.purse.amount;
    }

    if (next.purse.currency === 'GBP') {
      nextAmount = this.convertToEuro(next.purse.amount);
    } else {
      nextAmount = next.purse.amount;
    }
    // console.log(
    //   'Next Price %s from Item %s and Currency %s',
    //   nextAmount,
    //   next.event.title,
    //   next.purse.currency
    // );
    // console.log(
    //   'Prev Price %s from Item %s and Currency %s',
    //   prevAmount,
    //   prev.event.title,
    //   prev.purse.currency
    // );
    if (prevAmount > nextAmount) {
      return -1;
    }

    if (prevAmount < nextAmount) {
      return 1;
    }
    return 0;
  }
}
