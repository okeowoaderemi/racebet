import { Component, OnInit } from '@angular/core';
import { IRaceData } from './next-race-box-interfaces';
import { RaceTypeFilterServices } from '../race-type-filter/race-type-filter.service';
import { NextRaceBoxServices } from './next-race-box-services';

@Component({
  selector: 'app-race-next',
  templateUrl: './next-race-box.component.html',
  styleUrls: ['./next-race-box.scss']
})
export class RaceBoxComponent implements OnInit {
  public raceData: IRaceData;
  constructor(
    private nextBoxServices: NextRaceBoxServices,
    private filterServices: RaceTypeFilterServices
  ) {
    this.filterServices.filterListener.subscribe(item => {
      if (item === null) {
        return;
      }
      this.nextBoxServices.setFilterData(item);
      const data = this.nextBoxServices.transform(
        this.nextBoxServices.getData()
      );
      this.handleData(data);
    });
  }
  ngOnInit() {
    this.nextBoxServices.load().subscribe(
      response => {
        this.nextBoxServices.setFilterData(this.filterServices.getFilter());
        this.nextBoxServices.setData(response.data);
        const data = this.nextBoxServices.transform(
          this.nextBoxServices.getData()
        );
        this.handleData(data);
      },
      error => {
        alert(error);
      }
    );
  }

  handleData(data) {
    this.raceData = data;

  }

  getSilkImage(silk: string) {
    if (silk === '' || silk.length === 0) {
      return;
    }
    return `assets/images/silks/${silk}`;
  }

  getRaceUrl(id_race: string) {
    return `http://www.racebets.com/bet/${id_race}`;
  }

  getCssRaceType(race_type: string) {
    return `type-${race_type.toLowerCase()}-i`;
  }

  toEuro(amount: number) {
    return this.nextBoxServices.convertToEuro(amount);
  }

  getCountryFlag(country: string) {
    return `icons flags ${country}`;
  }

  countDown(time: number) {
    // get total seconds between the times
    const now = Date.now();
    const timeTravelTarget = time * 1000;

    if (now > timeTravelTarget) {
      return 'Due';
    }

    let offsetTime = Math.abs(timeTravelTarget - now) / 1000;

    const days = Math.floor(offsetTime / 86400);
    offsetTime -= days * 86400;

    // calculate (and subtract) whole hours
    const hours = Math.floor(offsetTime / 3600) % 24;
    offsetTime -= hours * 3600;

    // calculate (and subtract) whole minutes
    const minutes = Math.floor(offsetTime / 60) % 60;
    offsetTime -= minutes * 60;

    return Math.abs(offsetTime) + 'mins';
  }
}
