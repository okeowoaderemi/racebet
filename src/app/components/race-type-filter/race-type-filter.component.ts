import { Component, OnInit } from '@angular/core';
import { IRaceContants, FILTER_RACE_TYPE } from './race-type-filter.constant';
import { RaceTypeFilterServices } from './race-type-filter.service';

export interface IRaceFilterStateTypes {
  isGallopFilterEnabled: boolean;
  isTrotFilterEnabled: boolean;
  isJumpFilterEnabled: boolean;
  isDogFilterEnabled: boolean;
}

@Component({
  selector: 'app-racebet-filter',
  styleUrls: ['./race-type-filter.scss'],
  templateUrl: './race-type-filter.component.html'
})
export class RaceFilterComponent implements OnInit {
  public constants?: IRaceContants;
  public states: IRaceFilterStateTypes = {
    isDogFilterEnabled: false,
    isJumpFilterEnabled: true,
    isGallopFilterEnabled: true,
    isTrotFilterEnabled: true
  };
  constructor(private filterServices: RaceTypeFilterServices) {
    this.constants = FILTER_RACE_TYPE;
  }
  ngOnInit() {}

  /**
   *  Alters the state of the filter and raises an event to trigger change
   * @param filterType The Filter Type
   */
  applyFilter(filterType: string): void {
    const filterOption: string = filterType.toUpperCase();
    switch (filterOption) {
      case FILTER_RACE_TYPE.DOG:
        this.states.isDogFilterEnabled = !this.states.isDogFilterEnabled;
        break;

      case FILTER_RACE_TYPE.GALLOP:
        this.states.isGallopFilterEnabled = !this.states.isGallopFilterEnabled;
        break;

      case FILTER_RACE_TYPE.TROT:
        this.states.isTrotFilterEnabled = !this.states.isTrotFilterEnabled;
        break;

      case FILTER_RACE_TYPE.JUMP:
        this.states.isJumpFilterEnabled = !this.states.isJumpFilterEnabled;
        break;


    }
    this.filterServices.setFilter(this.states);
    this.filterServices.broadcast(this.filterServices.getFilter());

  }
}
