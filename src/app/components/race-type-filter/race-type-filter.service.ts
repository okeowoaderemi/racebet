/**
 * @description RaceTypeServices the purpose of the class is to manage the filter states
 * and also be used to get the current active states when they are triggered
 */

import { Injectable } from '@angular/core';
import { IRaceFilterStateTypes } from './race-type-filter.component';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RaceTypeFilterServices {
  private filterEventEmitter: BehaviorSubject<any>;
  public filterListener: Observable<any>;


  private filter: IRaceFilterStateTypes = {
    isDogFilterEnabled: true,
    isGallopFilterEnabled: true,
    isJumpFilterEnabled: true,
    isTrotFilterEnabled: true
  };

  constructor() {
    this.filterEventEmitter = new BehaviorSubject<any>(null);
    this.filterListener = this.filterEventEmitter.asObservable();
  }

  /**
   * Sets the current filter for the application
   * @param {IRaceFilterStateTypes} filter
   * @memberof RaceTypeFilterServices
   */
  setFilter(filter: IRaceFilterStateTypes) {
    this.filter = filter;
  }
  /**
   * Retrieves the current filter from the service
   * @returns {IRaceFilterStateTypes}
   * @memberof RaceTypeFilterServices
   */
  getFilter(): IRaceFilterStateTypes {
    return this.filter;
  }

  broadcast(filter: IRaceFilterStateTypes) {
    this.filterEventEmitter.next(filter);
  }
}
