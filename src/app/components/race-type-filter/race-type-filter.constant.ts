export interface IRaceContants {
  TROT: string;
  GALLOP: string;
  JUMP: string;
  DOG: string;
}
export const FILTER_RACE_TYPE: IRaceContants = {
  TROT: 'T',
  GALLOP: 'G',
  JUMP: 'J',
  DOG: 'D'
};
