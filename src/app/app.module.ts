import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RaceFilterComponent } from './components/race-type-filter/race-type-filter.component';
import { RaceBoxComponent } from './components/next-race-box/next-race-box.component';
import { RaceTypeFilterServices } from './components/race-type-filter/race-type-filter.service';
import { NextRaceBoxServices } from './components/next-race-box/next-race-box-services';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [AppComponent, RaceFilterComponent, RaceBoxComponent],
  imports: [BrowserModule, HttpClientModule],
  providers: [RaceTypeFilterServices, NextRaceBoxServices],
  bootstrap: [AppComponent]
})
export class AppModule {}
