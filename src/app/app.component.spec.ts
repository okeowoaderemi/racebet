import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { RaceBoxComponent } from 'app/components/next-race-box/next-race-box.component';
import { RaceFilterComponent } from 'app/components/race-type-filter/race-type-filter.component';
import { RaceTypeFilterServices } from 'app/components/race-type-filter/race-type-filter.service';
import { NextRaceBoxServices } from 'app/components/next-race-box/next-race-box-services';
import { By } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        RaceBoxComponent,
        RaceFilterComponent
      ],
      imports: [
        HttpClientModule
      ],
      providers: [
        RaceTypeFilterServices,
        NextRaceBoxServices
      ]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));
  it(`should have as title 'Racebet'`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('Racebet');
  }));

  it(`It should have the racebox component`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const raceboxDOM = fixture.debugElement.query(By.css('app-race-next'));
    expect(raceboxDOM).not.toBeNull();
  }));

  it(`It should have the FilterType component`, async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const raceboxDOM = fixture.debugElement.query(By.css('app-racebet-filter'));
    expect(raceboxDOM.nativeElement).not.toBeNull();
  }));
});
